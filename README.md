# Karionette
Node.js IRC bot
Forked from: https://github.com/Deide/Marionette

## Dependencies & Install
```
On Linux/*NIX, you will need 'curl' installed for web fetches.
>Debian/Ubuntu~ $ sudo apt-get install curl
```

> * Copy "config.example" to "config" and edit appropriately.
> * Copy the "DEFAULT_data" directory to "data"; there are things you can edit in here, too.
> * Once the bot is up and running, add a new user as admin by doing ;adduser <user> <pass> <secret code from your config file>

## ISSUES
If you have any problems, feel free to drop by #pyoshi on irc.esper.net and ask.
