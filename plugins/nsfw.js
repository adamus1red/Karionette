/* global irc bot config sql logger */
"use strict";
var db = new sql.Database("porn.db");

bot.command({
	command: "nsfw",
	help: "Will give you a random NSFW image",
	syntax: config.command_prefix+"nsfw [id] - Example: "+config.command_prefix+"roll 1157 \"returns image with the ID 1157 (optional)\"",
	callback: function (input) {
		if(typeof input.args !== "undefined") {
			db.get("SELECT rowid, * FROM images WHERE rowid = ? LIMIT 1;", input.args[0] ,function(err, res) {
				logger.debug(JSON.stringify(res));
				if(err) {
					logger.error(JSON.stringify(err))
					irc.say(input.context, input.nick+": Error getting image");	
				} else {
					irc.say(input.context, input.nick+": ID-"+ res.rowid  + " " + res.link);	
				}
			});	
		} else {
			db.get("SELECT rowid, * FROM images ORDER BY RANDOM() LIMIT 1;", function(err, res) {
				if (err) {
					logger.error(JSON.stringify(err))
					irc.say(input.context, input.nick+": Error getting image");
				} else {
					irc.say(input.context, input.nick+": ID-"+ res.rowid  + " " + res.link);
				}
			});	
		}
	}
});

bot.command({
	command: "add",
	help: "Add a NSFW image.",
	syntax: config.command_prefix+" add <link> [<link>,...] - Example: "+config.command_prefix+"add http://i.imgur.com/removed.png",
	callback: function (input) {
		var uchk = db.prepare('SELECT * FROM "users" WHERE nick = ?;');
		uchk.get(input.nick.toLowerCase(),function(err,row) {
			logger.debug(JSON.stringify(row));
			if(err){
				logger.error(JSON.stringify(err))
				irc.say(input.context, input.nick+": Error authorizing");
			} else if(typeof row !== "undefined"){
				if(row.nick.toLowerCase() === input.nick.toLowerCase()){
					var stmt = db.prepare('INSERT INTO "main"."images" ("link","addedBy") VALUES (?,?);');
					logger.debug("Adding images")
					for(var i=0; i<input.args.length; i++){
						stmt.run(input.args[i].toLowerCase(),input.nick.toLowerCase());
						logger.debug("image added");
						irc.say(input.context, input.nick+": added: "+ input.args[i]);
					}
				} else {
					irc.say(input.context, input.nick+": you ain't authorized to do dat");
				}
			} else {
				irc.say(input.context, input.nick+": you ain't authorized to do dat");
			}
		});
	}
});
bot.command({
	command: "count",
	help: "count the number of NSFW images.",
	syntax: config.command_prefix+"count - Example: "+config.command_prefix+"count",
	callback: function (input) {
		db.all("SELECT * FROM images;", function(err, res) {
			if (err) {
				logger.error(JSON.stringify(err))
				irc.say(input.context, input.nick+": Error getting image");
			} else {
				irc.say(input.context, input.nick+": "+ res.length + " images in the database");
			}
		});
		
	}
});

bot.command({
	command: "auth",
	help: "Manage authorizations for NSFW images",
	syntax: config.command_prefix+"auth <add|remove> <nick> - Example: "+config.command_prefix+"auth add MrRandom",
	callback: function (input) {
		var uchk = db.prepare('SELECT * FROM "users" WHERE nick = ? AND canAdd = 1;');
		try {
			uchk.get(input.nick.toLowerCase(),function(err,row) {
				
				if(err){
					logger.error(JSON.stringify(err));
					irc.say(input.context, input.nick.toLowerCase()+": Error authorizing");
				} else {
					logger.debug(JSON.stringify(row));
					if(row.nick.toLowerCase() === input.nick.toLowerCase()){
						if(input.args[0].toLowerCase() === "add") {
							var stmt = db.prepare('INSERT INTO "users" ("nick") VALUES (?);');
							logger.debug("Added user ", input.args[1].toLowerCase());
							logger.debug(JSON.stringify(input, null, 4));
							stmt.run(input.args[1].toLowerCase());
							irc.say(input.context, input.nick+": added user");
						} else if(input.args[0].toLowerCase() === "remove"){
							var stmt = db.prepare('DELETE FROM "users" WHERE "nick" = ?;');
							if(input.args[1].toLowerCase().match(/(MrRandom|fryerfly|starbuck)/i)){
								irc.say(input.context, input.nick+": thats unpossible");
							} else {
								logger.debug("Removed user ", input.args[1].toLowerCase());
								stmt.run(input.args[1].toLowerCase());
								irc.say(input.context, input.nick+": user removed");
							}
						} else {
							irc.say(input.context, input.nick+": that ain't a command!");
						}
					} else {
						irc.say(input.context, input.nick+": you ain't authorized to do dat");
					}
				}
			});
		}
		catch (ex) {
			logger.debug(ex);
		}
	}
});