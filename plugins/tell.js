﻿// Tell someone something on join if saved message for them
"use strict";
var msgDB = new DB.Json({filename: "messages"}),
	messages = msgDB.getAll();

function checkMessages(nick, context) {
	var i, l, send, lnick = nick.toLowerCase();
	if (!messages[lnick]) return;
	i = 0; l = messages[lnick].length;
	for (; i < l; i++) {
		if (messages[lnick][i].channel) {
			if (context[0] === "#" && context.toLowerCase() === messages[lnick][i].channel) {
				if (!send) send = [];
				if (messages[lnick][i].time !== undefined) {
					messages[lnick][i].message = messages[lnick][i].nick+", message from "+messages[lnick][i].from+" ("+
						lib.duration(messages[lnick][i].time, false, true)+" ago): "+messages[lnick][i].message;
				}
				send.push([ messages[lnick][i].method, context, messages[lnick][i].message ]);
				messages[lnick].splice(i, 1); i--; l--;
			}
		} else {
			if (!send) send = [];
			send.push([ messages[lnick][i].method, nick, messages[lnick][i].message ]);
			messages[lnick].splice(i, 1); i--; l--;
		}
	}
	if (send && send.length) {
		irc.rated(send);
		if (!messages[lnick].length)
			delete messages[lnick];
		msgDB.saveAll(messages);
	}
}

function addMessage(message) {
	var lnick = message.nick.toLowerCase();
	if (!messages[lnick])
		messages[lnick] = [];
	messages[lnick].push(message);
	msgDB.saveAll(messages);
}

bot.event({
	handle: "messageQueueListener",
	event: "Event: queueMessage",
	callback: addMessage
});

bot.event({
	handle: "messageMsg",
	event: "PRIVMSG",
	callback: function (input) {
		checkMessages(input.nick, input.context);
	}
});

bot.event({
	handle: "messageJoin",
	event: "JOIN",
	callback: function (input) {
		checkMessages(input.nick, input.context);
	}
});

bot.event({
	handle: "messageNick",
	event: "NICK",
	callback: function (input) {
		setTimeout(function () {
			ial.Channels(input.newnick).forEach(function (channel) {
				checkMessages(input.newnick, channel);
			});
		}, 250); // <- making sure IAL is updated first
	}
});

bot.command({
	command: "tell",
	help: "Passes along a message when the person person in question is spotted next.",
	syntax: config.command_prefix+"tell <nick> <message> - Example: "+config.command_prefix+
		"tell ranma your pantsu are lovely 1/2 the time.",
	arglen: 2,
	callback: function (input) {
		if (!input.channel) {
			irc.say(input.context, "tell can only be used in channels.");
			return;
		}
		if (new RegExp(config.nick+"|"+input.nick, "i").test(input.args[0])) {
			irc.say(input.context, "Nope.");
			return;
		}
		addMessage({
			method: "say",
			nick: input.args[0],
			from: input.nick,
			channel: input.context,
			message: input.args.slice(1).join(" "),
			time: new Date().valueOf()
		});
		irc.say(input.context, "I'll tell them when I see them next.");
	}
});
